function afficherAlphabet() {
    //fonction qui retourne l'alphabet
    let lettre = ""; //variable pour stocker les lettres   
    let chaineRetour = ""; //variable de retour
    for (let i = 65; i < 65 + 26; i++) { //pour i allant de 65 à 91 (nombre unicode correspondant à A et Z)
        lettre = String.fromCharCode(i); //conversion du nombre unicode en caractère
        chaineRetour += lettre + " "; //ajout de la lettre à la chaine de caractère de retout
    }
    return chaineRetour; //on retourne le résultat
}

let alphabet = afficherAlphabet(); //variable qui contient la valeur de retour de la fonction
console.log(alphabet); //affichage en console du résultat