$(document).ready(function() {
    //événement de clique sur le bouton ajouter
    $("#btnAdd").click(AjouterLigne);
    //événement de clique sur le bouton supprimer
    $("#btnDel").click(SupprimerLigne);

    //fonction liée à l'événement de clique sur le bouton ajouter
    function AjouterLigne() {
        //ajoute une ligne contenant le numéro de cette ligne au tableau 

        //compte le nombre de lignes présentes et ajout de 1 pour le numéro actuel
        let nbreLigne = $("#bodyTable tr").length + 1;
        //ajout de la ligne à la fin du body du tableau
        $("#bodyTable").append("<tr><td>" + nbreLigne + "</td></tr>")

        //appel de la fonction CalculerTotal()
        let somme = CalculerTotal();
        //ajout du résultat en html
        $("#bodyFoot").html("<tr><td>" + somme + "</td></tr>")
    }

    //fonction liée à l'événement de clique sur le bouton supprimer
    function SupprimerLigne() {
        //supprime la dernière ligne du body du tableau
        $("#bodyTable tr:last-child").remove();

        //appel de la fonction CalculerTotal()
        let somme = CalculerTotal();
        //ajout du résultat en html
        $("#bodyFoot").html("<tr><td>" + somme + "</td></tr>")
    }

    //fonction de calcul de la somme des numéros du tableau
    function CalculerTotal() {
        let total = 0;
        //fait une boucle sur un élément de la page
        $("#bodyTable td").each(function(index) {
                //somme du nombre de boucle effectuée
                total += parseInt($(this).html());
            })
            //on retourne le résultat
        return total;
    }
})