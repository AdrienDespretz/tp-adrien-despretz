function afficherFibonacci(prmNbreVal) {
    //fonction qui retourne les prmNbreVal premières valeurs de la suite de fibonacci
    let vectFibo = [0, 1]; //initialisation du vecteur avec ses 2 premières valeurs
    for (let i = 2; i < prmNbreVal; i++) { //pour i allant de 2 à prmNbreVal
        vectFibo[i] = vectFibo[i - 2] + vectFibo[i - 1]; //la valeur suivante prend la somme des 2 précédentes
    }
    return vectFibo; //retourne le vecteur
}

const nbreVal = 17; //constante pour choisir le nbre de valeurs que l'on veut
let affFibo = afficherFibonacci(nbreVal); //variable contenant la valeur de retour de la fonction
console.log(affFibo); //affichage du résultat en console