/* 
 * File:   MainAcqui.cpp
 * Author: adrien
 *
 * Created on 27 mars 2020, 17:08
 */

#include <cstdlib>
#include <iostream>
#include "UtilAcqui.h"

using namespace std;

int main(int argc, char** argv) {
    
    float vectLect[NB_MAX_ACQUI] = {10,20,30,40,50};
    
    Acqui objAcqui;
    objAcqui.Initialiser();
    
    objAcqui.AjouterValeur(1.0);
    objAcqui.AjouterValeur(-8.2);
    objAcqui.AjouterValeur(5.3);
    objAcqui.AjouterValeur(10);
    objAcqui.AjouterValeur(6.7);
    
    objAcqui.LireMesures(vectLect);

    return 0;
}

