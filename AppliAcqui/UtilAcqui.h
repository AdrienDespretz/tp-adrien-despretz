/* 
 * File:   Acqui.h
 * Utilitaire : classe "Acqui"
 * Classe permettant l'enregistrement de mesures dans un vecteur
 * 
 * Author: adrien
 *
 * Created on 27 mars 2020, 17:08
 */

#ifndef UTILACQUI_H
#define UTILACQUI_H

// taille du vecteur d'acquisition = nombre max de mesures possibles
#define NB_MAX_ACQUI 5                          //5 pour debug

class Acqui {
private:
    float vectAcqui[NB_MAX_ACQUI];              //vecteur d'acquisition de données
    int nbAcqui;                                //nombre de valeurs acquises
    float moy;                                  //moyenne des valeurs du vecteur
    
    //calcul la moyenne du vecteur
    float CalculerMoyenne(float prmVect[], int prmTaille) ;

public:
    Acqui();                                    //constructeur
    ~Acqui();                                   //destructeur
    
    int AjouterValeur(float prmVal);            //ajoute une valeur dans le vecteur
    
    int LireNbAcqui(void);                      //retourne la valeur de nbAcqui
    float LireMoyenne(void);                    //retourne la valeur de la moyenne
    
    //Cette fonction initialise les « prmTaille » premières cases du vecteur 
    //avec la valeur « prmVal ».
    void Initialiser(float prmVal = 0, unsigned int prmTaille = NB_MAX_ACQUI);
    
    //Cette fonction copie le contenu des « nbAcqui » première cases du vecteur 
    //« vectAcqui[] » dans le vecteur « prmVectLect[] ».
    int LireMesures(float prmVectLect[]);
};
#endif /* UTILACQUI_H */

