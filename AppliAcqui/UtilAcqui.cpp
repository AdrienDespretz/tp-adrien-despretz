/* 
 * File:   Acqui.cpp
 * Fichier d'implémentation de la classe "Acqui"
 * Classe permettant l'enregistrement de mesures dans un vecteur
 * 
 * Author: adrien
 * 
 * Created on 27 mars 2020, 17:08
 */

#include "UtilAcqui.h"

Acqui::Acqui() {
    //contructeur : initialisation des attributs
    nbAcqui = 0;
    moy = 0;
    for (int i = 0; i < NB_MAX_ACQUI; i++) {
        vectAcqui[i] = 0;
    }
}

int Acqui::AjouterValeur(float prmVal) {
    //Ajoute une valeur au vecteur d'acquisition à la suite des précédentes
    //si il y a de la place
    //prmVal : valeur à ajouter dans le vecteur d'acquisition

    int valRetour = -1; //si l'ajout a pu se faire, nombre de valeurs utiles, sinon -1
    bool valAjouter = false; //défini si prmVal a été ajouté ou non
    //boucle de passage dans le vecteur
    for (int i = 0; i < NB_MAX_ACQUI; i++) {
        //si la position actuelle dans le vecteur n'est pas une valeur utile 
        //et si prmVal n'a pas été ajouté
        if ((vectAcqui[i] == 0) && (valAjouter != true)) {
            vectAcqui[i] = prmVal; //on ajoute prmVal à cette position du vecteur
            valAjouter = true; //prmVal a été ajouté donc passe à true
            nbAcqui += 1; //on ajoute 1 à la valeur de retour
        }
    }
    //calcul de la moyenne
    moy = CalculerMoyenne(vectAcqui, nbAcqui);

    //si la valeur a pu être ajouté
    if (valAjouter == true) {
        valRetour = nbAcqui;
    }
    return valRetour;
}

float Acqui::CalculerMoyenne(float prmVect[], int prmTaille) {
    //calcul et retourne la moyenne du vecteur d'acquisition si cela est possible, 
    //sinon retourne -1000
    float moyenne = 0; //variable de calcul de la moyenne
    float valRetour = -1000; //valeur de retour
    //si le nombre de valeurs à calculer est différent de 0
    if (prmTaille != 0) {
        //boucle de passage dans le vecteur
        for (int i = 0; i < prmTaille; i++) {
            moyenne += prmVect[i]; //somme des valeurs à calculer
        }
        moyenne = moyenne / prmTaille; //moyenne = somme divisé par nbre de valeur
        valRetour = moyenne; //valeur de retour prend la valeur de la moyenne
    }
    return valRetour; //on retourne la valeur de retour
}

int Acqui::LireNbAcqui() {
    return nbAcqui;
}

float Acqui::LireMoyenne() {
    return moy;
}

void Acqui::Initialiser(float prmVal, unsigned int prmTaille) {
    //initialise toutes les cases du vecteur d'acquisition avec prmVal
    //il sera considéré comme vide (nbAcqui = 0 et moy = 0)
    unsigned int nbCases = NB_MAX_ACQUI;
    nbAcqui = 0;
    //si le nombre de cases à changer est supérieur au nombre de case du vecteur
    if (prmTaille > nbCases) {
        prmTaille = NB_MAX_ACQUI; //nb cases à changer = nb cases du vecteur
    }
    //boucle de passage dans le vecteur pour l'initialiser
    for (int i = 0; i < prmTaille; i++) {
        vectAcqui[i] = prmVal;
    }
    moy = 0;
}

int Acqui::LireMesures(float prmVectLect[]) {
    //Cette fonction copie le contenu des « nbAcqui » première cases du vecteur 
    //« vectAcqui[] » dans le vecteur « prmVectLect[] ». L’ordre des valeurs est
    //respecté. La fonction retourne le nombre de valeurs effectivement copiées.

    int cpt = 0; //compte le nombre de valeurs copiées

    //boucle de passage dans le vecteur pour copier les valeurs
    for (int i = 0; i < nbAcqui; i++) {
        prmVectLect[i] = vectAcqui[i]; //copie de la valeur à l'index i
        cpt++; //incrémentation du compteur
    }
    return cpt; //on retourne le nombre de valeurs copiées
}

Acqui::~Acqui() {
    //destructeur
}