/* 
 * File:   MainVecteurExo2.cpp
 * Author: Adrien Despretz
 * 
 * Affiche le contenu d'un vecteur, calcul de sa valeur moyenne, recherche des
 * valeurs minimales et maximales ainsi que leur posisition
 *
 * Created on 21 mars 2020, 13:45
 */

#include <cstdlib>
#include <iostream>                         //cin, cout, endl

using namespace std;

#define TAILLE_VECT 10                      //taille du vecteur

int main(int argc, char** argv) {
    int somme = 0;                          //somme des nombres du vecteur
    float moyenne = 0;                      //valeur moyenne du vecteur
    unsigned int posMin = 0;                //position valeur minimale
    unsigned int posMax = 0;                //position valeur maximale

    //déclaration et initialisation du vecteur
    int vectEntier[TAILLE_VECT] = {-5, 2, 8, -10, 3, 0, -1, 5, 2, 1};

    /////////////////////////////CALCUL MOYENNE/////////////////////////////
    //boucle de calcul de la somme des nombres du vecteur
    for (int i = 0; i < TAILLE_VECT; i++) {
        somme += vectEntier[i];
    }
    //utilisation de (float) devant somme pour pouvoir avoir un résultat en float
    moyenne = (float) somme / TAILLE_VECT;

    ////////////////////////////VALEUR MINIMALE/////////////////////////////
    //boucle pour trouver la valeur minimal
    for (int i = 1; i < TAILLE_VECT - 1 ; i++){
        if(vectEntier[i] < vectEntier[posMin]){
            posMin = i;
        }
    }
    
    ////////////////////////////VALEUR MAXIMALE/////////////////////////////
    //boucle pour trouver la valeur minimal
    for (int i = 1; i < TAILLE_VECT; i++){
        if(vectEntier[i] > vectEntier[posMax]){
            posMax = i;
        }
    }
    
    ///////////////////////////////AFFICHAGE///////////////////////////////
    //boucle d'affichage du vecteur
    for (int i = 0; i < TAILLE_VECT; i++) {
        cout << vectEntier[i] << " ";
    }
    cout << endl; //retour à la ligne

    //Affichage de la valeur moyenne
    cout << "Valeur moyenne = " << moyenne << endl;
    
    //Affichage de la valeur minimale
    cout << "Position de la valeur minimale : " << posMin + 1  << endl;
    cout << "Valeur minimal = " << vectEntier[posMin] << endl;
    
    //Affichage de la valeur maximale
    cout << "Position de la valeur maximale : " << posMax + 1  << endl;
    cout << "Valeur maximale = " << vectEntier[posMax] << endl;

    return 0;
}