$(document).ready(function() {
    //fonction slider poids
    $("#idSliderPoids").on('input', function() {
        //appel de la fonction
        afficherIMC();
    })

    //fonction slider taille
    $("#idSliderTaille").on('input', function() {
        //appel de la fonction
        afficherIMC();
    })

    //fonction de clique pour afficher l'imc
    function afficherIMC() {
        //on récupère les valeurs saisies
        let poids = $("#idSliderPoids").val();
        let taille = $("#idSliderTaille").val();

        //affichage du poids à côté du slider
        $("#textPoids").html(poids);
        //affichage de la taille à côté du slider
        $("#textTaille").html(taille);

        //affichage de l'imc
        $("#textIMC").html(calculerIMC(poids, taille) + " (" + interpreterIMC(calculerIMC(poids, taille)) + ")");
    }

    //fonction de calcul de l'imc
    function calculerIMC(prmPoids, prmTaille) {
        //calcul de l'imc
        let valRetour = prmPoids / ((prmTaille / 100) * (prmTaille / 100));

        //formatage de l'affichage pour avoir un seul chiffre après la virgule
        valRetour = valRetour.toFixed(1);

        //retour du résultat
        return valRetour;
    }

    //fonction de l'interprétation de l'imc
    function interpreterIMC(prmValIMC) {
        let interpretation = "";
        if (prmValIMC < 16.5) {
            interpretation = "dénutrition";
        }
        if ((prmValIMC >= 16.5) && (prmValIMC < 18.5)) {
            interpretation = "maigreur";
        }
        if ((prmValIMC >= 18.5) && (prmValIMC < 25)) {
            interpretation = "corpulence normale";
        }
        if ((prmValIMC >= 25) && (prmValIMC < 30)) {
            interpretation = "surpoids";
        }
        if ((prmValIMC >= 30) && (prmValIMC < 35)) {
            interpretation = "obésité modérée";
        }
        if ((prmValIMC >= 35) && (prmValIMC < 40)) {
            interpretation = "obésité sévère";
        }
        if (prmValIMC >= 40) {
            interpretation = "obésité morbide";
        }
        return interpretation;
    }
})