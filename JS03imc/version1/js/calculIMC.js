$(document).ready(function() {
    //bouton calcul imc
    $("#btnCalculIMC").click(afficherIMC);

    //fonction de clique pour afficher l'imc
    function afficherIMC() {
        //on récupère les valeurs saisies
        let poids = $("#idPoids").val();
        let taille = $("#idTaille").val();

        //si les valeurs saisies ne sont pas des nombre on affiche un message d'erreur
        if (isNaN(poids) || isNaN(taille)) {
            alert("Saisie incorrecte");
        } else {
            //on remplace les virgules par des points
            poids = poids.replace(",", ".");
            taille = taille.replace(",", ".");

            //on force la conversion en nombre
            poids = Number(poids);
            taille = Number(taille);

            //affichage de l'imc
            $("#textIMC").html(calculerIMC(poids, taille));
        }
    }

    //fonction de calcul de l'imc
    function calculerIMC(prmPoids, prmTaille) {
        //calcul de l'imc
        let valRetour = prmPoids / ((prmTaille / 100) * (prmTaille / 100));

        //formatage de l'affichage pour avoir un seul chiffre après la virgule
        valRetour = valRetour.toFixed(1);

        //retour du résultat
        return valRetour;
    }
})