$(document).ready(function() {
    //fonction slider poids
    $("#idSliderPoids").on('input', function() {
        //appel de la fonction
        afficherIMC();
    });

    //fonction slider taille
    $("#idSliderTaille").on('input', function() {
        //appel de la fonction
        afficherIMC();
    });

    //fonction btn radio
    $("input[name=sexe]").on('change', function() {
        afficherIMC();
    });

    //fonction de clique pour afficher l'imc
    function afficherIMC() {
        //on récupère les valeurs saisies
        let poids = $("#idSliderPoids").val();
        let taille = $("#idSliderTaille").val();
        let imc = calculerIMC(poids, taille);
        let interpretation = interpreterIMC(imc);
        let aiguille = afficherBalance(imc);

        //affichage du poids à côté du slider
        $("#textPoids").html(poids);
        //affichage de la taille à côté du slider
        $("#textTaille").html(taille);

        //affichage de l'imc
        $("#textIMC").html(imc + " (" + interpretation + ")");

        //affichage de la balance
        $("#aiguille").css("left", aiguille + "px");

        //affichage de la silhouette
        afficherSilhouette(imc);

    }

    //fonction de calcul de l'imc
    function calculerIMC(prmPoids, prmTaille) {
        //calcul de l'imc
        let valRetour = prmPoids / ((prmTaille / 100) * (prmTaille / 100));

        //formatage de l'affichage pour avoir un seul chiffre après la virgule
        valRetour = valRetour.toFixed(1);

        //retour du résultat
        return valRetour;
    }

    //fonction de l'interprétation de l'imc
    function interpreterIMC(prmValIMC) {
        let interpretation = "";
        if (prmValIMC < 16.5) {
            interpretation = "dénutrition";
        }
        if ((prmValIMC >= 16.5) && (prmValIMC < 18.5)) {
            interpretation = "maigreur";
        }
        if ((prmValIMC >= 18.5) && (prmValIMC < 25)) {
            interpretation = "corpulence normale";
        }
        if ((prmValIMC >= 25) && (prmValIMC < 30)) {
            interpretation = "surpoids";
        }
        if ((prmValIMC >= 30) && (prmValIMC < 35)) {
            interpretation = "obésité modérée";
        }
        if ((prmValIMC >= 35) && (prmValIMC < 40)) {
            interpretation = "obésité sévère";
        }
        if (prmValIMC >= 40) {
            interpretation = "obésité morbide";
        }
        return interpretation;
    }

    //fonction du déplacement de l'aiguille sur la balance
    function afficherBalance(prmValIMC) {
        if ((prmValIMC >= 10) && (prmValIMC <= 45)) {
            let deplacement = (60 / 7) * prmValIMC - (600 / 7); //en pixels
            return deplacement;
        }
    }


    //fonction qui gère l'affichage des différentes silhouette
    function afficherSilhouette(prmValIMC) {
        let decalage = 0; //decalage de l'image pour afficher différentes silhouette
        const tailleSilhouette = 105; //taille en pixel d'une silhouette

        //on récupère le choix du sexe
        let sexe = $("input[name=sexe]:checked").val();

        //on affiche la silhouette selon le sexe choisi
        if (sexe === "homme") {
            $("#silhouette").css("background-image", "url(css/img/IMC-homme.jpg)");
        } else {
            $("#silhouette").css("background-image", "url(css/img/IMC-femme.jpg)");
        }

        //choix de la silhouette en fonction de l'imc
        if ((prmValIMC >= 16.5) && (prmValIMC < 18.5)) {
            decalage = 5 * tailleSilhouette;
        }
        if ((prmValIMC >= 18.5) && (prmValIMC < 25)) {
            decalage = 4 * tailleSilhouette;
        }
        if ((prmValIMC >= 25) && (prmValIMC < 30)) {
            decalage = 3 * tailleSilhouette;
        }
        if ((prmValIMC >= 30) && (prmValIMC < 35)) {
            decalage = 2 * tailleSilhouette;
        }
        if (prmValIMC >= 35) {
            decalage = tailleSilhouette;
        }

        //affichage de la silhouette correspondant à l'imc
        $("#silhouette").css("background-position", decalage);
    }
});